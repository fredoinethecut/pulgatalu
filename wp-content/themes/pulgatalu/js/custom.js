/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Init Menu
4. Init Search
5. Init Gallery
6. Init Google Map


******************************/

$(document).ready(function() {
  "use strict";

  /*

	1. Vars and Inits

	*/

  var map;
  var header = $(".header");
  var menu = $(".menu");
  var menuActive = false;

  setHeader();

  $(window).on("resize", function() {
    setHeader();
  });

  $(document).on("scroll", function() {
    setHeader();
  });

  initMenu();
  initSearch();
  initGallery();
  initGoogleMap();

  /*

	2. Set Header

	*/

  function setHeader() {
    if ($(window).scrollTop() > 91) {
      header.addClass("scrolled");
    } else {
      header.removeClass("scrolled");
    }
  }

  /*

	3. Init Menu

	*/

  function initMenu() {
    if ($(".hamburger").length && $(".menu").length) {
      var hamb = $(".hamburger");

      hamb.on("click", function() {
        if (!menuActive) {
          // 1. Hamburgeri clicki nupp, kust avab
          openMenu();
        } else {
          // 2. Hambrugeri close, kui on avatud
          closeMenu();
        }
      });
    }
  }

  function openMenu() {
    menu.addClass("active");
    menuActive = true;
  }

  function closeMenu() {
    menu.removeClass("active");
    menuActive = false;
  }

  /*

	4. Init Search

	*/

  function initSearch() {
    if ($(".search_dropdown").length) {
      var dds = $(".search_dropdown");
      dds.each(function() {
        var dd = $(this);
        if (dd.find("ul > li").length) {
          var ddItems = dd.find("ul > li");
          dd.on("click", function() {
            dd.toggleClass("active");
          });
          ddItems.each(function() {
            var ddItem = $(this);
            ddItem.on("click", function() {
              dd.find("span").text(ddItem.text());
            });
          });
        }
      });
    }
  }

  /*

	5. Init Gallery

	*/

  function initGallery() {
    if ($(".gallery_slider").length) {
      var gallerySlider = $(".gallery_slider");
      gallerySlider.owlCarousel({
        items: 5,
        // paneb galerii automaatselt liikuma!
        autoplay: true,
        // See loop kui panna true peale, siis hakkab duubeldama pilte ja saab lõpmatuseni scrollida
        loop: false,
        // tekitab prev/next buttoni alla kust galeriid scrollida
        nav: false,
        // paneb prev/next buttoni asemele dotid
        dots: false,
        // kui kiirelt galerii valge osa tagasi liigub kylgedelt
        smartSpeed: 1000,
        // Pildite omavaheline kaugus galeriis ---->
        margin: 8,
        // Mitu pilti on ekraanil vastavalt pikslitele ---->
        responsive: {
          0: {
            items: 1
          },
          576: {
            items: 2
          },
          768: {
            items: 3
          },
          992: {
            items: 4
          },
          1440: {
            items: 6
          }
        }
      });
    }

    if ($(".gallery_item").length) {
      $(".colorbox").colorbox({
        rel: "colorbox",
        photo: true,
        maxWidth: "95%",
        maxHeight: "95%"
      });
    }
  }
});
