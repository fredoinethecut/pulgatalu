<?php
add_action( 'wp_enqueue_scripts', 'mat_assets' );
function mat_assets() {
	// 1. enqueue all plugins
	// 2. enqueue all custom scripts
	wp_enqueue_style( 'main', get_template_directory_uri() . '/dist/main.css' );
	wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/src/styles/bootstrap-4.1.2/bootstrap.min.css' );
	wp_enqueue_style( 'about_responsive', get_template_directory_uri() . '/src/styles/about_responsive.css' );
	wp_enqueue_style( 'about', get_template_directory_uri() . '/src/styles/about.css' );
	wp_enqueue_style( 'blog_responsive', get_template_directory_uri() . '/src/styles/blog_responsive.css' );
	wp_enqueue_style( 'blog', get_template_directory_uri() . '/src/styles/blog.css' );
	wp_enqueue_style( 'contact_responsive', get_template_directory_uri() . '/src/styles/contact_responsive.css' );
	wp_enqueue_style( 'contact', get_template_directory_uri() . '/src/styles/contact.css' );
	wp_enqueue_style( 'main_styles', get_template_directory_uri() . '/src/styles/main_styles.css' );
	wp_enqueue_style( 'peamaja', get_template_directory_uri() . '/src/styles/peamaja.css' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/src/styles/responsive.css' );
	wp_enqueue_style( 'rooms_responsive', get_template_directory_uri() . '/src/styles/rooms_responsive.css' );
	wp_enqueue_style( 'rooms', get_template_directory_uri() . '/src/styles/rooms.css' );
	wp_enqueue_style( 'fawesome', get_template_directory_uri() . '/plugins/font-awesome-4.7.0/css/font-awesome.css' );
	wp_enqueue_style( 'owl', get_template_directory_uri() . '/plugins/OwlCarousel2-2.2.1/owl.carousel.css' );
	wp_enqueue_style( 'owltheme', get_template_directory_uri() . '/plugins/OwlCarousel2-2.2.1/owl.theme.default.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/plugins/OwlCarousel2-2.2.1/animate.css' );
	wp_enqueue_style( 'colorbox', get_template_directory_uri() . '/plugins/colorbox/colorbox.css' );




	// wp_enqueue_script( 'index', get_template_directory_uri() . '/index.js' );
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js' );
	wp_enqueue_script( 'popper', get_template_directory_uri() . '/src/styles/bootstrap-4.1.2/popper.js', array('jquery') );
	wp_enqueue_script( 'bootsmin', get_template_directory_uri() . '/src/styles/bootstrap-4.1.2/bootstrap.min.js', array('jquery') );
	wp_enqueue_script( 'TweenMax.min', get_template_directory_uri() . '/plugins/greensock/TweenMax.min.js', array('jquery') );
	wp_enqueue_script( 'TimelineMax.min', get_template_directory_uri() . '/plugins/greensock/TimelineMax.min.js', array('jquery') );
	wp_enqueue_script( 'scrollmagic', get_template_directory_uri() . '/plugins/scrollmagic/ScrollMagic.min.js', array('jquery') );
	wp_enqueue_script( 'animationgsap', get_template_directory_uri() . '/plugins/greensock/animation.gsap.min.js', array('jquery') );
	wp_enqueue_script( 'own.carousel', get_template_directory_uri() . '/plugins/OwlCarousel2-2.2.1/owl.carousel.js', array('jquery') );
	wp_enqueue_script( 'easing', get_template_directory_uri() . '/plugins/easing/easing.js', array('jquery') );
	wp_enqueue_script( 'progressbar.min', get_template_directory_uri() . '/plugins/progressbar/progressbar.min.js', array('jquery') );
	wp_enqueue_script( 'colorbox', get_template_directory_uri() . '/plugins/colorbox/jquery.colorbox-min.js', array('jquery') );
	wp_enqueue_script( 'parallax.min', get_template_directory_uri() . '/plugins/parallax-js-master/parallax.min.js', array('jquery') );
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/js/custom.js', array('jquery'));


}
