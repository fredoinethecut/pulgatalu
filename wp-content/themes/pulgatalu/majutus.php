<?php

/**
 * Template Name: Majutus template
 */
?>

<?php get_header(); ?>



<!-- Menu -->

<div class="menu">
	<div class="background_image" style="background-image:url(../wp-content/themes/pulgatalu/images/menu.jpg)"></div>
	<div class="menu_content d-flex flex-column align-items-center justify-content-center">
		<ul class="menu_nav_list text-center">
			<li>
				<a href="<?php echo home_url(); ?>"><?php _e('Pealeht', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="meist"><?php _e('Meist', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="majutus"><?php _e('Majutus', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="kontakt"><?php _e('Kontakt', 'fredos-text-domain'); ?></a>
			</li>
		</ul>
	</div>
</div>

<!-- Home -->

<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="../wp-content/themes/pulgatalu/images/pulgailus.jpg" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content text-center">
						<div class="home_title">
							<h1>
								<?php _e('Majutus', 'fredos-text-domain'); ?>
							</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Rooms -->

<div class="rooms">
	<div class="container">
		<div class="row rooms_row">
			<!-- Room -->
			<div class="col-lg-6">
				<a href="https://www.booking.com/hotel/ee/pulga-holiday-complex.et.html?label=gen173bo-1DCAsoQkIVcHVsZ2EtaG9saWRheS1jb21wbGV4SAtYA2hCiAEBmAELuAEHyAEM2AED6AEB-AECiAIBmAICqAIDuAKSx4PrBcACAQ&sid=6806027d3559ba44b11028b1a062a07a&dest_id=31251&dest_type=city&hapos=1&hpos=1&sr_order=popularity&srepoch=1566630823&srpvid=d41432d346030078&ucfs=1&srhp=1&ref_is_wl=1&activeTab=room_110009501_2&selected_rate=110009501" target="_blank">
					<div class="rooms_item">
						<div class="rooms_image">
							<img src="../wp-content/themes/pulgatalu/images/twin.jpg" />
						</div>
						<div class="rooms_title_container text-center">
							<div class="rooms_title">
								<h1><?php _e('Kahe inimese tuba rõduga', 'fredos-text-domain'); ?></h1>
							</div>
							<div class="rooms_price">
								<span>58€</span> /2in</div>
						</div>
						<div class="rooms_list">
							<ul>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>
										<div><?php _e('Magamistuba:', 'fredos-text-domain'); ?></div>
									</div>
									<div><?php _e('1 lai kaheinimese voodi', 'fredos-text-domain'); ?></div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>
										<div><?php _e('Toa suurus:', 'fredos-text-domain'); ?></div>
									</div>
									<div>32m²</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>
										<div><?php _e('WC/dušš', 'fredos-text-domain'); ?></div>
									</div>
									<div><?php _e('toas', 'fredos-text-domain'); ?></div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>
										<div><?php _e('Lisavoodi:', 'fredos-text-domain'); ?></div>
									</div>
									<div>15EUR</div>
								</li>
							</ul>
						</div>
				</a>
				<div class="button rooms_button ml-auto mr-auto">
					<a href="#"><?php _e('Vaata lähemalt!', 'fredos-text-domain'); ?></a>
				</div>
			</div>
		</div>

		<!-- Room -->
		<div class="col-lg-6">
			<a href="https://www.booking.com/hotel/ee/pulga-holiday-complex.et.html?label=gen173bo-1DCAsoQkIVcHVsZ2EtaG9saWRheS1jb21wbGV4SAtYA2hCiAEBmAELuAEHyAEM2AED6AEB-AECiAIBmAICqAIDuAKSx4PrBcACAQ&sid=6806027d3559ba44b11028b1a062a07a&dest_id=31251&dest_type=city&hapos=1&hpos=1&sr_order=popularity&srepoch=1566630823&srpvid=d41432d346030078&ucfs=1&srhp=1&ref_is_wl=1&activeTab=room_110009502_12&selected_rate=110009502" target="_blank">
				<div class="rooms_item">
					<div class="rooms_image">
						<img src="../wp-content/themes/pulgatalu/images/saunamaja.jpg" alt="https://unsplash.com/@niklanus" />
					</div>
					<div class="rooms_title_container text-center">
						<div class="rooms_title">
							<h1><?php _e('Puhkemaja', 'fredos-text-domain'); ?></h1>
						</div>
						<div class="rooms_price">
							<span>280€</span> <?php _e('/öö', 'fredos-text-domain'); ?></div>
					</div>
					<div class="rooms_list">
						<ul>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>
									<div><?php _e('Mahutavus', 'fredos-text-domain'); ?></div>
								</div>
								<div>12 <?php _e('Täiskasvanut', 'fredos-text-domain'); ?></div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>
									<div><?php _e('Maja suurus:', 'fredos-text-domain'); ?></div>
								</div>
								<div>210m²</div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>
									<div><?php _e('WC/dušš:', 'fredos-text-domain'); ?></div>
								</div>
								<div><?php _e('Esimesel korrusel', 'fredos-text-domain'); ?></div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>
									<div><?php _e('Lisavoodi:', 'fredos-text-domain'); ?></div>
								</div>
								<div>15€</div>
							</li>
						</ul>
					</div>
			</a>
			<div class="button rooms_button ml-auto mr-auto">
				<a href="#"><?php _e('Vaata lähemalt:', 'fredos-text-domain'); ?></a>
			</div>
		</div>
	</div>

	<!-- Room -->
	<div class="col-lg-6">
		<a href="https://www.booking.com/hotel/ee/pulga-holiday-complex.et.html?label=gen173bo-1DCAsoQkIVcHVsZ2EtaG9saWRheS1jb21wbGV4SAtYA2hCiAEBmAELuAEHyAEM2AED6AEB-AECiAIBmAICqAIDuAKSx4PrBcACAQ&sid=6806027d3559ba44b11028b1a062a07a&dest_id=31251&dest_type=city&hapos=1&hpos=1&sr_order=popularity&srepoch=1566630823&srpvid=d41432d346030078&ucfs=1&srhp=1&ref_is_wl=1&activeTab=room_110009503_3&selected_rate=110009503" target="_blank">
			<div class="rooms_item">
				<div class="rooms_image">
					<img src="../wp-content/themes/pulgatalu/images/peretuba.jpg" alt="https://unsplash.com/@niklanus" />
				</div>
				<div class="rooms_title_container text-center">
					<div class="rooms_title">
						<h1><?php _e('Mugavustega peretuba rõduga', 'fredos-text-domain'); ?></h1>
					</div>
					<div class="rooms_price">
						<span>69€</span> /3in</div>
				</div>
				<div class="rooms_list">
					<ul>
						<li class="d-flex flex-row align-items-start justify-content-start">
							<div>
						</li>
						<li class="d-flex flex-row align-items-start justify-content-start">
							<div>
								<div><?php _e('Magamistuba:', 'fredos-text-domain'); ?></div>
							</div>
							<div><?php _e('1 kitsas voodi või üheinimese', 'fredos-text-domain'); ?></div>
						</li>
						<li class="d-flex flex-row align-items-start justify-content-start">
							<div>
								<div>T<?php _e('Toa suurus:', 'fredos-text-domain'); ?></div>
							</div>
							<div>32m²</div>
						</li>
						<li class="d-flex flex-row align-items-start justify-content-start">
							<div>
								<div><?php _e('WC/dušš:', 'fredos-text-domain'); ?></div>
							</div>
							<div><?php _e('Toas', 'fredos-text-domain'); ?></div>
						</li>
						<li class="d-flex flex-row align-items-start justify-content-start">
							<div>
								<div><?php _e('Lisavoodi:', 'fredos-text-domain'); ?></div>
							</div>
							<div>15EUR</div>
						</li>
					</ul>
				</div>
		</a>
		<div class="button rooms_button ml-auto mr-auto">
			<a href="#"><?php _e('Vaata lähemalt!', 'fredos-text-domain'); ?></a>
		</div>
	</div>
</div>

<!-- Room -->
<div class="col-lg-6">
	<a href="https://www.booking.com/hotel/ee/pulga-holiday-complex.et.html?label=gen173bo-1DCAsoQkIVcHVsZ2EtaG9saWRheS1jb21wbGV4SAtYA2hCiAEBmAELuAEHyAEM2AED6AEB-AECiAIBmAICqAIDuAKSx4PrBcACAQ&sid=6806027d3559ba44b11028b1a062a07a&dest_id=31251&dest_type=city&hapos=1&hpos=1&sr_order=popularity&srepoch=1566630823&srpvid=d41432d346030078&ucfs=1&srhp=1&ref_is_wl=1&activeTab=room_110009504_4&selected_rate=110009504" target="_blank">
		<div class="rooms_item">
			<div class="rooms_image">
				<img src="../wp-content/themes/pulgatalu/images/apartment.jpg" alt="https://unsplash.com/@niklanus" />
			</div>
			<div class="rooms_title_container text-center">
				<div class="rooms_title">
					<h1>Apartment</h1>
				</div>
				<div class="rooms_price">
					<span>110€</span> /4in</div>
			</div>
			<div class="rooms_list">
				<ul>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Magamistuba:', 'fredos-text-domain'); ?></div>
						</div>
						<div><?php _e('1 lai kaheinimese voodi, laste voodi kuni 10a', 'fredos-text-domain'); ?></div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Magamistuba 2:', 'fredos-text-domain'); ?></div>
						</div>
						<div><?php _e('2 üheinimesevoodit', 'fredos-text-domain'); ?></div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Köök/elutuba:', 'fredos-text-domain'); ?></div>
						</div>
						<div>pliit, külmkapp, ahi, nõudepesumasin, kõik köögitarbed, kamin, terass.</div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('WC/dušš:', 'fredos-text-domain'); ?></div>
						</div>
						<div>ühine</div>
					</li>
				</ul>
			</div>
	</a>
	<div class="button rooms_button ml-auto mr-auto">
		<a href="#"><?php _e('Vaata lähemalt!', 'fredos-text-domain'); ?></a>
	</div>
</div>
</div>

<!-- Room -->
<div class="col-lg-6">
	<a href="https://www.booking.com/hotel/ee/pulga-holiday-complex.et.html?label=gen173bo-1DCAsoQkIVcHVsZ2EtaG9saWRheS1jb21wbGV4SAtYA2hCiAEBmAELuAEHyAEM2AED6AEB-AECiAIBmAICqAIDuAKSx4PrBcACAQ&sid=6806027d3559ba44b11028b1a062a07a&dest_id=31251&dest_type=city&hapos=1&hpos=1&sr_order=popularity&srepoch=1566630823&srpvid=d41432d346030078&ucfs=1&srhp=1&ref_is_wl=1&activeTab=room_110009505_7&selected_rate=110009505" target="_blank">
		<div class="rooms_item">
			<div class="rooms_image">
				<img src="../wp-content/themes/pulgatalu/images/aidamaja.jpg" alt="https://unsplash.com/@niklanus" />
			</div>
			<div class="rooms_title_container text-center">
				<div class="rooms_title">
					<h1><?php _e('Aidamaja', 'fredos-text-domain'); ?></h1>
				</div>
				<div class="rooms_price">
					<span>112€</span> /öö</div>
			</div>
			<div class="rooms_list">
				<ul>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Mahutavus:', 'fredos-text-domain'); ?></div>
						</div>
						<div><?php _e('7 täiskasvanut', 'fredos-text-domain'); ?></div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Ruumi suurus:', 'fredos-text-domain'); ?></div>
						</div>
						<div>43m²</div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div>Vaade:</div>
						</div>
						<div>Loodus</div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Lisavoodi:', 'fredos-text-domain'); ?></div>
						</div>
						<div>15€</div>
					</li>
				</ul>
			</div>
	</a>
	<div class="button rooms_button ml-auto mr-auto">
		<a href="#"><?php _e('Vaata lähemalt!', 'fredos-text-domain'); ?></a>
	</div>
</div>
</div>

<!-- Room -->
<div class="col-lg-6">
	<a href="https://www.booking.com/hotel/ee/pulga-holiday-complex.et.html?label=gen173bo-1DCAsoQkIVcHVsZ2EtaG9saWRheS1jb21wbGV4SAtYA2hCiAEBmAELuAEHyAEM2AED6AEB-AECiAIBmAICqAIDuAKSx4PrBcACAQ&sid=6806027d3559ba44b11028b1a062a07a&dest_id=31251&dest_type=city&hapos=1&hpos=1&sr_order=popularity&srepoch=1566630823&srpvid=d41432d346030078&ucfs=1&srhp=1&ref_is_wl=1&activeTab=room_110009506_4&selected_rate=110009506" target="_blank">
		<div class="rooms_item">
			<div class="rooms_image">
				<img src="../wp-content/themes/pulgatalu/images/peretuba.jpg" alt="https://unsplash.com/@niklanus" />
			</div>
			<div class="rooms_title_container text-center">
				<div class="rooms_title">
					<h1><?php _e('Sauna rent', 'fredos-text-domain'); ?></h1>
				</div>
				<div class="rooms_price">
					<span>40€</span> /2h</div>
			</div>
			<div class="rooms_list">
				<ul>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Mahutavus:', 'fredos-text-domain'); ?></div>
						</div>
						<div><?php _e('Leiliruum 2 & Eesruum 6', 'fredos-text-domain'); ?></div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('Ruumi suurus:', 'fredos-text-domain'); ?></div>
						</div>
						<div>22m²</div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div><?php _e('WC/dušš:', 'fredos-text-domain'); ?></div>
						</div>
						<div>Saunas</div>
					</li>
					<li class="d-flex flex-row align-items-start justify-content-start">
						<div>
							<div>Midagi veel lisada?:</div>
						</div>
						<div>Extra</div>
					</li>
				</ul>
			</div>
	</a>
	<div class="button rooms_button ml-auto mr-auto">
		<a href="#"><?php _e('Vaata lähemalt!', 'fredos-text-domain'); ?></a>
	</div>
</div>
</div>
</div>
</div>
</div>

<?php get_footer();
