<?php

/**
 * Template Name: Homepage template
 */
?>

<?php get_header(); ?>

<body>
	<div class="super_container">


		<!-- Home -->


		<div class="home front-page">
			<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/pulgatalu5.jpg" data-speed="0.8"></div>
			<div class="home_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="home_content text-center">
								<div class="home_title">
									<h1>
										<?php _e('Mönus puhkus Saaremaa looduses', 'fredos-text-domain'); ?>
									</h1>
								</div>
								<div class="home_text">
									<?php _e('', 'fredos-text-domain'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Intro -->

		<div class="intro">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="section_title_container text-center">
							<div class="section_title">
								<h1>
									<?php _e('Pulga talu - Rohkem kui puhkus', 'fredos-text-domain'); ?>
								</h1>
							</div>
							<div class="section_text">
								<?php _e('Pulga talu asub Lõuna-Saaremaal, Nässuma külas. Pakume oma külalistele erinevaid puhkamis- ja majutusvõimalusi. Ööbimiseks
								saab valida kõikide mugavustega toa, perepuhkuseks sobiliku köögi ja söögitoaga apartmendi, saunamaja või arhailise
								aida vahel. Voodikohad 46le ja lisavoodite võimalus. Peosaal mahutab kuni 80 inimest.', 'fredos-text-domain'); ?>
								<br>
								<br>
								<?php _e('Meie ruumid sobivad nii pulmadeks, suvepäevadeks, juubeliteks kui seminaride korraldamiseks. Meil on grillimaja,
								lõkkeplats ja võrkpalliväljak. Korraldame loodusmatkasid, mille käigus on võimalus tutvuda ka meie talu Islandi tõugu
								lammaste ning Herefordi tõugu veistega.', 'fredos-text-domain'); ?>
								<br>
								<br>
								<?php _e('2015.aastal saadud toetuse abil rajasime ürdiaia ja aiamaja ning renoveerisime ajaloolise keldri ning puurkaevu.
								Samuti soetasime suitsuahjud ning videoprojektori. Nüüd saame pakkuda ka külalistele rohkem teenuseid - võimalusi
								toidu valmistamisel ja ürituste korraldamisel. Rõõmuga pakume kohapeal valmistatud saaremaist suitsukala ning roogasid
								kohapeal kasvanud aedviljadest ja maitsetaimedest.', 'fredos-text-domain'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row intro_row">
					<!-- Intro Content -->
					<div class="col-lg-6 intro_col">
						<div class="intro_content">
							<div class="intro_author d-flex flex-row align-items-center justify-content-start"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Facilities -->

		<div class="facilities">
			<div class="container">
				<div class="row icon_box_row">
					<!-- Icon Box -->
					<div class="col-lg-4">
						<div class="icon_box text-center">
							<div class="icon_box_icon">
								<img src="wp-content/themes/pulgatalu/images/icon_1.svg" alt="https://www.flaticon.com/authors/monkik" />
							</div>
							<div class="icon_box_title">
								<h2><?php _e('Majutus & Toitlustus', 'fredos-text-domain'); ?></h2>
							</div>
							<div class="icon_box_text">
								<p>


									<?php _e('Pulga puhkekompleks on maaliline roogkatuste ja kiviaedadega koht Pihtla vallas, Nässuma poolsaarel,
									kus pidada pulmi, firmapidusid, seminare, juubeleid ja muid üritusi.
									Pulga puhkekompleksis on lisaks avarale peosaalile, mis mahutab kuni 80 inimest ka majutuse võimalus 50le,
									kaks sauna, laululava, grillikoda , ilus ja avar õueala suurte terassidega.
									Meie majutustingimused on kaasaegsed ja mugavad.', 'fredos-text-domain'); ?></br> </br>

									<?php _e('Toitlustus on ainult ettetellimisel <b>eve@cavere.ee</b> või telefonil <b>+372 513 4457</b>', 'fredos-text-domain'); ?> </br> </br>


									<?php _e('Pakume kõikide mugavustega numritubetube ja ka lihtsamaid majutusvõimalusi.
									Peamajas on mugavustega toad, vastuvõturuum ja seminarisaal.
									Saunamajas on magamiseks 3 tuba, puhkeruum, puuküttega saun, infrasaun. Sobib ööbimiseks seltskonnale kuni 12 inimest.
									Saunamajas on kamina ja kööginurgaga puhkeruum. Tube saab ka eraldi üürida, samuti on võimalik üürida sauna ilma öömajata.
									Suur leiliruum saunamajas mahutab palju häid saunalisi.
									<b>Väike saun mahutab saunalavale 2 inimest ja eesruumi kuni 6 inimest, võimalik üürida ettetellimisel.</b>
									Vanaaegne ait on mõeldud magamiseks kuni 7-le inimesele.
									Kompleksis on majutuse ja köögiga peomaja seminarideks ja pidudeks.', 'fredos-text-domain'); ?> </br></br>

								</p>
							</div>
						</div>
					</div>

					<!-- Icon Box -->
					<div class="col-lg-4">
						<div class="icon_box text-center">
							<div class="icon_box_icon">
								<img src="wp-content/themes/pulgatalu/images/icon_2.svg" alt="https://www.flaticon.com/authors/monkik" />
							</div>
							<div class="icon_box_title">
								<h2><?php _e('Aktiivne Tegevus', 'fredos-text-domain'); ?></h2> </br>
							</div>
							<div class="icon_box_text">
								<p class="icon-textbox">
									<h4><?php _e('Loodusmatkad', 'fredos-text-domain'); ?></h4>

									<p><?php _e('Tutvumine poollooduslike kooslustega. Merematkad.', 'fredos-text-domain'); ?></p></br></br>

									<h4><?php _e('Klassiekskursioon', 'fredos-text-domain'); ?></h4>

									<p><?php _e('Pakume koolilastele aktiivset tegevust, õppereise loodusesse ja tutvumist kariloomadega.', 'fredos-text-domain'); ?></p></br></br>

									<h4><?php _e('Telkimine ja autokaravanid', 'fredos-text-domain'); ?></h4>


									<p><?php _e('Lahkesti ootame ka rännumehi ja pereturiste, kes soovivad Saaremaa kaunis looduses telkida või autokaravaniga matkata.
									Telgi saab püsti panna Pulga puhkekompleksi territooriumile sobivasse kohta kooskõlastatult perenaisega.
									Autokaravanidele on muruplats, kus on ka võimalik saada elektrit.', 'fredos-text-domain'); ?></p></br> </br>



									<p><?php _e('Hinnad:
									Telgi maksumus 15.00€/ööpäev
									Hind sisaldab dussi ja wc kasutamist. Hommikusöögi saab lisaks tellida meie vastuvõtus ja hind ühele inimesele on 9.00€.
									Autokaravan 20.00€/ööpäev, sisaldab dussi ja wc kasutamist.', 'fredos-text-domain'); ?></p>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Gallery -->



		<?php $images = get_field('galerii'); ?>

		<?php if ($images) : ?>
			<div class="gallery">
				<div class="gallery_slider_container">
					<!-- Gallery Slider -->
					<div class="owl-carousel owl-theme gallery_slider">
						<?php foreach ($images as $image) : ?>

							<!-- Slide -->
							<div class="owl-item gallery_item">
								<a class="colorbox" href="<?php echo $image['url']; ?>">
									<div class="background_image" style="background-image:url(<?php echo $image['sizes']['large']; ?>)"></div>
								</a>
								<div class="gallery_overlay trans_200 d-flex flex-column align-items-center justify-content-center">
								</div>
							</div>

						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>




		<!-- Newsletter -->

		<div class="newsletter">
			<div class="container">
				<div class="row">
					<div class="maaeluimg">
					</div>
					<div class="col">
						<h3>
							<?php _e('Investeeringud majandustegevuse mitmekesistamiseks maapiirkonnas mittepõllumajandusliku tegevuse suunas (MAK 2014-2020 MEEDE
							6.4) 2016', 'fredos-text-domain'); ?>
						</h3>
						<ul class="maaelu">
							<li></li>
							<li><?php _e('Ait, saunamaja, teenindushoone ja puhkemaja parendamine', 'fredos-text-domain'); ?></li>
							<li><?php _e('Sauna ümberehitamine', 'fredos-text-domain'); ?></li>
							<li><?php _e('Diiselgeneraatori ja jahutusagregaadi ostmine', 'fredos-text-domain'); ?></li>
						</ul>
					</div>
				</div>
				<div class="row newsletter_row">
					<!-- Newsletter Form -->
					<div class="col-lg-6 newsletter_col"></div>
				</div>
			</div>
		</div>
</body>


<?php get_footer();
