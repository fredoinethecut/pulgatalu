<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Pulga</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	</script>
</head>
<!-- Header -->
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="header_content d-flex flex-row align-items-center justify-content-start">
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<div>Pulga</div>
						</a>
						</a>
					</div>
					<nav class="main_nav">
						<ul class="d-flex flex-row align-items-center justify-content-start">
							<li class>
								<a href="<?php echo home_url(); ?>"><?php _e('Pealeht', 'fredos-text-domain'); ?></a>
							</li>
							<li>
								<a href="meist"><?php _e('Üritused', 'fredos-text-domain'); ?></a>
							</li>
							<li>
								<a href="majutus"><?php _e('Majutus', 'fredos-text-domain'); ?></a>
							</li>
							<li>
								<a href="kontakt"><?php _e('Kontakt', 'fredos-text-domain'); ?></a>
							</li>
						</ul>
					</nav>
					<div class="hamburger ml-auto">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<!-- Menu -->
<div class="menu">
	<div class="background_image" style="background-image:url(wp-content/themes/pulgatalu/images/menu.jpg)"></div>
	<div class="menu_content d-flex flex-column align-items-center justify-content-center">
		<ul class="menu_nav_list text-center">
			<li>
				<a href="<?php echo home_url(); ?>"><?php _e('Pealeht', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="meist"><?php _e('Meist', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="majutus"><?php _e('Majutus', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="kontakt"><?php _e('Kontakt', 'fredos-text-domain'); ?></a>
			</li>
		</ul>
	</div>
</div>
