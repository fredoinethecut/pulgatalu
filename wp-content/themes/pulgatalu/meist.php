<?php

/**
 * Template Name: Meist template
 */
?>

<?php get_header(); ?>

<!-- Menu -->

<div class="menu">
	<div class="background_image" style="background-image:url(../wp-content/themes/pulgatalu/images/menu.jpg)"></div>
	<div class="menu_content d-flex flex-column align-items-center justify-content-center">
		<ul class="menu_nav_list text-center">
			<li>
			<li>
				<a href="<?php echo home_url(); ?>"><?php _e('Pealeht', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="meist"><?php _e('Üritused', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="majutus"><?php _e('Majutus', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="kontakt"><?php _e('Kontakt', 'fredos-text-domain'); ?></a>
			</li>
		</ul>
	</div>
</div>

<!-- Home -->

<div class="home">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="../wp-content/themes/pulgatalu/images/yritused.jpg" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content text-center">
						<div class="home_title">
							<h1>Üritused</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Facilities -->

<div class="facilities">
	<div class="container">
		<div class="row icon_box_row">
			<!-- Icon Box -->
			<div class="col-lg-4">
				<div class="icon_box text-center">
					<div class="information">
						<div class="information-image">
						</div>
						<div class="information-image2">
						</div>
					</div>
					<div class="icon_box_title">
						<h2><?php _e('Pulmad', 'fredos-text-domain'); ?></h2>
					</div>
					<div class="icon_box_text">
						<p><?php _e('
								Pulmade pidamiseks on meil suur saal 120m2 ideaalne 80le inimesele, voodikohad 46le(lisavooditega 50le).
								Saalis on lauad ja toolid. Pulga puhkekompleksi köögis valmib maitsev eine, kuid soovi korral võib
								pruutpaar ka väljaspoolt tellida neile sobiva cateringi teenuses.
								Toitlustamise tellimisel meie poolt koostame näidismenüü kolme erineva variandi ja hinnaklassiga, et oleks
								lihtsam otsustada, mida külalistele pakkuda. Menüü koostamisel asetame põhirõhu sellele, et külalised saaksid
								lauda istudes kõhud täis. Muude soovide korral saame asjad läbirääkida. Alkoholi osas palume
								meile edastada margi eelistused, millele teeme hinnapakkumise, tasutakse kulupõhiselt.
								Soovi korral võib ka noorpaar joogid cateringilt tellida, kuid sel juhul lisandub "korgitasu".
								Ilusa ilma korral katame šampuse laua ja tordilaua väliterassil, muul ajal saalis või siseterassil.
								Proovime omalt poolt kõik teha, et pulmapäev tuleks meeldejääv ning vääriks meenutamist ka aastate pärast.</br></br>

								Pulga puhkekompleks töötab enamuse aastast ettetellimisel ja konkreetse hinnapakkumise saame teha pärast teiepoolset päringut.
								Päring palume saata e-posti aadressile <b>eve@cavere.ee.</b> Kui on soov tulla kohapeale kohta vaatama, palun eelnevalt aeg kokku leppida
								telefonil <b>51 34457</b>.
								', 'fredos-text-domain'); ?></p>
					</div>
				</div>
			</div>

			<!-- Icon Box -->
			<div class="col-lg-4">
				<div class="icon_box text-center">
					<div class="information">
						<div class="information-image">
							<img src="../wp-content/themes/pulgatalu/images/ait.jpg">
						</div>
						<div class="information-image2">
							<img src="../wp-content/themes/pulgatalu/images/ait.jpg">
						</div>
					</div>
					<div class="icon_box_title">
						<h2><?php _e('Seminarid', 'fredos-text-domain'); ?></h2>
					</div>
					<div class="icon_box_text">
						<p><?php _e('
								Pulga puhkekompleksis saab korraldada seminare ja koolituspäevi. Seminariruum mahutab kuni 90 inimest,
								ümarlauana sobilik 65le inimesele. Lauad ja toolid paigutatakse vastavalt inimeste arvule,
								et oleks mugav ja hea esinejaid jälgida. Pauside ajal saab istuda suveterassil, soovi korral saab jalutada meie ilusal õuel . </br></br>

								Seminaride korraldamiseks pakume täisteenust: </br>
								● toitlustust, kohvipause </br>
								● seminarijärgseid tegevusi: saunaõhtut kuuma leili, massažiga (ettetellimisel) </br>
								● majutust </br></br>

								Seminariruumi hinnas on WiFi, videoprojektor, ekraan ja pabertahvel.</br>
								Toitlustuse päringu vastavalt seltskonna soovidele palume saata e-posti aadressile <b>eve@cavere.ee</b> </br></br>

								Pulga puhkekompleks töötab enamuse aastast ettetellimisel ja konkreetse hinnapakkumise saame teha pärast teiepoolset päringut.
								Päringu palume edastada e-posti aadressile <b>eve@cavere.ee</b>
																', 'fredos-text-domain'); ?></p>
					</div>
				</div>
			</div>

			<!-- Icon Box -->
			<div class="col-lg-4">
				<div class="icon_box text-center">
					<div class="information">
						<div class="information-image">
							<img src="../wp-content/themes/pulgatalu/images/ait.jpg">
						</div>
						<div class="information-image2">
							<img src="../wp-content/themes/pulgatalu/images/ait.jpg">
						</div>
					</div>
					<div class="icon_box_title">
						<h2><?php _e('Suvepäevad', 'fredos-text-domain'); ?></h2>
					</div>
					<div class="icon_box_text">
						<p><?php _e('
								Pulga puhkekompleks on väga hea koht suveseminaride ja suvepäevade pidamiseks.
								Meil on suur õueala ja laululava, kus on võimalik korraldada nii sportlike mänge kui ka etendusi ja kontserte.
								Suvepäevadele võib kaasa võtta oma peokorraldajad ja bändi, loomulikult võib küsida kontakte ka meie käest.
								Üks populaarsemaid suvepäevade üritusi on matk Nässuma poolsaare majaka juurde.
								Matka käigus saab tutvuda meie talu sõbralike islandi tõugu lammaste ja herefordi tõugu veistega.
								Soovi korral on võimalik majaka all pidada pikniku ning selleks vajaliku toitlustuse ja joogipoolise
								pakime teile eelneva tellimuse alusel kaasa. Matk Nässuma majaka juurde toimub jalgsi ja kestab 2,5-4 h. </br></br>

								Pulga puhkekompleksis on kaks sauna. Suurem neist on soome saun. Saun asub eraldi saunamajas,
								kus on olemas eesruum kamina ja kööginurga ning laua ja toolidega väiksema, kuni 20 inimese seltskonna jaoks.
								Leiliruumi mahub korraga umbes 10 inimest, saunas on karastamiseks külmaveeauk. Väiksem, infrapunasaun mahutab korraga umbes 4 inimest.</br></br>

								Pulga puhkekompleks töötab enamuse aastast ettetellimisel ja konkreetse hinnapakkumise saame teha pärast teiepoolset päringut.
								Päringu palume edastada e-posti aadressile <b>eve@cavere.ee</b>
								', 'fredos-text-domain'); ?></p>
					</div>
				</div>
			</div>

			<!-- Icon Box -->
			<div class="col-lg-4">
				<div class="icon_box text-center">
					<div class="information">
						<div class="information-image">
							<img src="../wp-content/themes/pulgatalu/images/ait.jpg">
						</div>
						<div class="information-image2">
							<img src="../wp-content/themes/pulgatalu/images/ait.jpg">
						</div>
					</div>
					<div class="icon_box_title">
						<h2><?php _e('Kontserdid', 'fredos-text-domain'); ?></h2>
					</div>
					<div class="icon_box_text">
						<p><?php _e('
							Ürituste jaoks on olemas oma kõlakoda, kus on ka kaasaaegne valgustus, sobib esinemiseks nii koorile kui bändile.
								', 'fredos-text-domain'); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<!-- Intro Right -->

<div class="intro_right intro">
	<div class="container">
		<div class="row row-xl-eq-height">

			<!-- Intro Right Content -->
			<div class="col-xl-6 intro_right_col">
				<div class="intro_right_content">
					<div class="sig text-center">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer();
