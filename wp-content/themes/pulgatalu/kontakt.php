<?php

/**
 * Template Name: Kontakt template
 */
?>

<?php get_header(); ?>


<!-- Menu -->

<div class="menu">
	<div class="background_image" style="background-image:url(../wp-content/themes/pulgatalu/images/menu.jpg)"></div>
	<div class="menu_content d-flex flex-column align-items-center justify-content-center">
		<ul class="menu_nav_list text-center">
			<li>
				<a href="<?php echo home_url(); ?>"><?php _e('Pealeht', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="meist"><?php _e('Meist', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="majutus"><?php _e('Majutus', 'fredos-text-domain'); ?></a>
			</li>
			<li>
				<a href="kontakt"><?php _e('Kontakt', 'fredos-text-domain'); ?></a>
			</li>
		</ul>
	</div>
</div>


<!-- Home -->

<!-- Google Map -->

<div id="map"></div>

<script>
	function initMap() {
		let location = {
			lat: 58.289000,
			lng: 22.808250
		};
		let map = new google.maps.Map(document.getElementById("map"), {
			zoom: 15,
			center: location,
		});
		var marker = new google.maps.Marker({
			map: map,
			title: 'Pulga'
		});
	}
</script>



<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvyXyfjdgbI0Ll6UH-hrlyg0BJXp6LlT0&callback=initMap" type="text/javascript"></script>







<!-- Contact -->

<div class="contact_section">
	<div class="container">
		<div class="row">
			<!-- Contact Content -->
			<div class="col-lg-5">
				<div class="contact_content">
					<div class="contact_logo">
						<div class="logo">
							<a href="#">
								<div>Pulga</div>
							</a>
						</div>
					</div>
					<div class="contact_section_text">
						<p>
							In vitae nisi aliquam, scelerisque leo a, volutpat sem. Vivamus rutrum dui fermentum eros hendrerit, id lobortis leo volutpat.
							Maecenas sollicitudin est in libero pretium interdum.
						</p>
					</div>
					<div class="contact_section_info">
						<ul>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div>
									<div class="d-flex flex-column align-items-center justify-content-center">
										<img src="../wp-content/themes/pulgatalu/images/icon_1.png" alt="" />
									</div>
								</div>
								<div>Nässumaa, Pulga</div>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div>
									<div class="d-flex flex-column align-items-center justify-content-center">
										<img src="../wp-content/themes/pulgatalu/images/icon_2.png" alt="" />
									</div>
								</div>
								<div><span><?php _e('Üritused Eve', 'fredos-text-domain'); ?></span><a href="tel:+372 513 4457"> +372 513 4457</a></div>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div>
									<div class="d-flex flex-column align-items-center justify-content-center">
										<img src="../wp-content/themes/pulgatalu/images/icon_2.png" alt="" />
									</div>
								</div>
								<div><span><?php _e('Administraator Tiina', 'fredos-text-domain'); ?></span><a href="tel: +372 519 79285"> +372 519 79285</a></div>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div>
									<div class="d-flex flex-column align-items-center justify-content-center">
										<img src="../wp-content/themes/pulgatalu/images/icon_3.png" alt="" />
									</div>
								</div>
								<div>info@pulga.ee</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<!-- Contact Image -->
			<div class="col-lg-7 contact_section_col">
				<div class="contact_image">
					<img src="../wp-content/themes/pulgatalu/images/pulgailus.jpg" />
				</div>
			</div>
		</div>
	</div>
</div>



<?php get_footer();
